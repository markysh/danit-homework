const togglePass = document.getElementById('password-toggle');
const pass = document.getElementById('password');
togglePass.addEventListener('click', () => {

    if (pass.type === 'password') {
        pass.type = 'text';
    } else {
        pass.type = 'password';
    }

    if (togglePass.classList.contains('fa-eye')) {
        togglePass.classList.remove('fa-eye');
        togglePass.classList.add('fa-eye-slash');
    } else {
        togglePass.classList.add('fa-eye');
        togglePass.classList.remove('fa-eye-slash');
    }
});

const confirmPassToggle = document.getElementById('confirm-password-toggle');
const confirmPass = document.getElementById('confirm-password');
confirmPassToggle.addEventListener('click', () => {

    if (confirmPass.type === 'password') {
        confirmPass.type = 'text';
    } else {
        confirmPass.type = 'password';
    }

    if (confirmPassToggle.classList.contains('fa-eye-slash')) {
        confirmPassToggle.classList.remove('fa-eye-slash');
        confirmPassToggle.classList.add('fa-eye');
    } else {
        confirmPassToggle.classList.add('fa-eye-slash');
        confirmPassToggle.classList.remove('fa-eye');
    }
});

const submitForm = document.getElementById('submit-form');
submitForm.addEventListener('submit',() => {

    const valid = document.getElementById('validation');
    valid.classList.add('hidden');

    if (pass.value === confirmPass.value) {
        alert('You are welcome!');
    } else {
        valid.classList.remove('hidden');
    }
});



















