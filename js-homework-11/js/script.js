function changeColorBtn() {

   document.addEventListener('keydown', function (event) {

       document.querySelectorAll('.btn').forEach( function (btn) {
            btn.classList.remove('active');
       });

       if (event.code === 'Enter') {
           document.querySelector('#btn-enter').classList.add('active');
       }

       if (event.code === 'KeyS') {
           document.querySelector('#btn-s').classList.add('active');
       }

       if (event.code === 'KeyE') {
           document.querySelector('#btn-e').classList.add('active');
       }

       if (event.code === 'KeyO') {
           document.querySelector('#btn-o').classList.add('active');
       }

       if (event.code === 'KeyN') {
           document.querySelector('#btn-n').classList.add('active');
       }

       if (event.code === 'KeyL') {
           document.querySelector('#btn-l').classList.add('active');
       }

       if (event.code === 'KeyZ') {
           document.querySelector('#btn-z').classList.add('active');
       }
   });
}

changeColorBtn();