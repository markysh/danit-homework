let inputNumber = document.querySelector('#inputNumber');
let priceContainer = document.querySelector('#priceContainer');
let validationContainer = document.querySelector('#validationContainer');

inputNumber.placeholder = 'Price';
inputNumber.addEventListener('blur', function (event) {
    let val = event.target.value;

    cleanUpValidation();

    if (val < 0) {
        inputNumber.classList.add('invalid');
        let invalidElement = createInvalidElement();
        validationContainer.appendChild(invalidElement);
        return;
    }

    let container = createPrice(val);

    priceContainer.appendChild(container);

});

function createPriceLabel(value) {
    let span = document.createElement('span');
    span.innerText = `Текущая цена: ${value}`;

    return span;
}

function createPriceButton() {
    let btn = document.createElement('button');
    btn.innerText = 'x';

    return btn;
}

function createPriceContainer(nodes) {
    let container = document.createElement('div');
    nodes.forEach(function (node) {
        container.appendChild(node);
    });

    return container;
}

function cleanUpValidation() {
    inputNumber.classList.remove('invalid');
    for (let i = 0; i < validationContainer.children.length; i++) {
        validationContainer.children[i].remove();
    }
}

function createPrice(val) {
    let span = createPriceLabel(val);
    let btn = createPriceButton();
    let container = createPriceContainer([span, btn]);

    btn.addEventListener('click', function () {
        container.remove();
        inputNumber.value = null;
    });

    return container;
}

function createInvalidElement() {
    let invalidText = document.createElement('span');
    invalidText.innerText = 'Please enter correct price';
    return invalidText;
}