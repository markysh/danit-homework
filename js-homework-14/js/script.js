let tab = function () {
    let tabLinks = $('.tabs-link-item'),
        tabContent = $('.tab');

    tabLinks.each((index, tab) => {
        $(tab).on('click', selectTabNav);
    });

    function selectTabNav() {
        tabLinks.each((index, tab) => {
            $(tab).removeClass('active');
        });

        const $tab = $(this);

        $tab.addClass('active');
        selectTabContent($tab.attr('data-tab-name'));
    }

    function selectTabContent(tabName) {
        tabContent.each((index, tab) => {
            const $tab = $(tab);
            $tab.hasClass(tabName) ? $tab.addClass('active') : $tab.removeClass('active');
        })
    }
};

tab();


