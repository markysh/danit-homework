let img = document.getElementById('img');
let imgArray = ["img/img1.jpg", "img/img2.jpg", "img/img3.jpg", "img/img4.jpg"];

let i = 1;

function changeImg() {
    img.setAttribute('src', imgArray[i]);
    i++;

    if (i >= imgArray.length) {
        i = 0;
    }
}

let sliderImg = setInterval(changeImg, 10000);

const startBtn = document.getElementById('start-btn');
const stopBtn = document.getElementById('stop-btn');

stopBtn.addEventListener('click', () => {
    clearInterval(sliderImg);
});

startBtn.addEventListener('click', () => {
   sliderImg = setInterval(changeImg, 10000)
});

