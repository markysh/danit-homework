function createNewUser() {
    const first = prompt('Enter first name', 'Olya');
    const last = prompt('Enter last name', 'Markysh');
    return {
        firstName: first,
        lastName: last,
        getLogin: getLogin
    };
    function getLogin() {
        const firstLetter = this.firstName.substr(0,1);
        const loginName = firstLetter + this.lastName;
        return loginName.toLowerCase();
    }
}
let user = createNewUser();


console.log(user.getLogin());
