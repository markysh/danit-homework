$("#home").on('click', function() {
    const scrollElement = $('.navbar');
    const offsetTop = scrollElement.offset().top;

    $('html').animate({ scrollTop: offsetTop }, 1000);
});

$("#arch").on('click', function() {
    const scrollElement = $('.background');
    const offsetTop = scrollElement.offset().top;

    $('html').animate({ scrollTop: offsetTop }, 1000);
});

$("#buildings").on('click', function() {
    const scrollElement = $('.thumbnail');
    const offsetTop = scrollElement.offset().top;

    $('html').animate({ scrollTop: offsetTop }, 1000);
});

$("#magazine").on('click', function() {
    const scrollElement = $('.clients');
    const offsetTop = scrollElement.offset().top;

    $('html').animate({ scrollTop: offsetTop }, 1000);
});

$("#blog").on('click', function() {
    const scrollElement = $('.rated');
    const offsetTop = scrollElement.offset().top;

    $('html').animate({ scrollTop: offsetTop }, 1000);
});

$("#news").on('click', function() {
    const scrollElement = $('.news');
    const offsetTop = scrollElement.offset().top;

    $('html').animate({ scrollTop: offsetTop }, 1000);
});

const scrollTopBtn = $('#scroller');
scrollTopBtn.on('click', () => {
    $('html').animate({ scrollTop: 0 }, 1000);
});

$(window).on('scroll', () => {
    if ($(window).scrollTop() > document.documentElement.clientHeight) {
        scrollTopBtn.addClass('active');
    } else {
        scrollTopBtn.removeClass('active');
    }
});