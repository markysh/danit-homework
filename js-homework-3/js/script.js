
let firstNumber = prompt('Enter first number', '1');
while (isNaN(firstNumber) || firstNumber === '' || firstNumber === null) {
    firstNumber = prompt('Enter first number again, please', '1')
}

let secondNumber = prompt('Enter second number', '2');
while (isNaN(secondNumber) || secondNumber === '' || secondNumber === null) {
    secondNumber = prompt('Enter second number again, please', '2')
}

let operation = prompt('Enter operation, please', '+');
while (operation !== '+' && operation !=='-' && operation !=="*" && operation !=="/") {
    operation = prompt('Enter operation again, please', '+')
}

function calc(first, second, operation) {
    if (operation === '+') {
        return add(first, second);
    }
    if (operation === '-') {
        return minus(first, second);
    }
    if (operation === '*') {
        return multiply(first, second);
    }
    if (operation === '/') {
        return div(first, second);
    }
}

console.log(calc(firstNumber, secondNumber, operation));


function add(value1, value2) {
    return value1 + value2;
}

function minus(value1, value2) {
    return value1 - value2;
}

function multiply(value1, value2) {
    return value1 * value2;
}

function div(value1, value2) {
    return value1 / value2;
}

