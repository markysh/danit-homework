
// SECTION OUR SERVICES
let tab = function () {
    let tabLink = document.querySelectorAll('.navbar-item'),
        tabContent = document.querySelectorAll('.services-navbar-content'),
        tabName;

    tabLink.forEach(item => {
        item.addEventListener('click', selectTabNav);
    });

    function selectTabNav() {
        tabLink.forEach(item => {
            item.classList.remove('active');
        });
        this.classList.add('active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }
    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
        })
    }
};

tab();

//SECTION WORK

//LOADING BTN

const loadMoreImages = [
    {
        src: 'img/graphic_design/graphic-design1.jpg',
        category: 'graphic_design',
        categoryName: 'Graphic Design'
    },
    {
        src: 'img/graphic_design/graphic-design2.jpg',
        category: 'graphic_design',
        categoryName: 'Graphic Design'
    },
    {
        src: 'img/graphic_design/graphic-design3.jpg',
        category: 'graphic_design',
        categoryName: 'Graphic Design'
    },
    {
        src: 'img/landing_page/landing-page1.jpg',
        category: 'landing_page',
        categoryName: 'Landing Page'
    },
    {
        src: 'img/landing_page/landing-page2.jpg',
        category: 'landing_page',
        categoryName: 'Landing Page'
    },
    {
        src: 'img/landing_page/landing-page3.jpg',
        category: 'landing_page',
        categoryName: 'Landing Page'
    },
    {
        src: 'img/web_design/web-design1.jpg',
        category: 'web_design',
        categoryName: 'Web Design'
    },
    {
        src: 'img/web_design/web-design2.jpg',
        category: 'web_design',
        categoryName: 'Web Design'
    },
    {
        src: 'img/web_design/web-design3.jpg',
        category: 'web_design',
        categoryName: 'Web Design'
    },
    {
        src: 'img/wordpress/wordpress1.jpg',
        category: 'wordpress',
        categoryName: 'Wordpress'
    },
    {
        src: 'img/wordpress/wordpress2.jpg',
        category: 'wordpress',
        categoryName: 'Wordpress'
    },
    {
        src: 'img/wordpress/wordpress3.jpg',
        category: 'wordpress',
        categoryName: 'Wordpress'
    }

];

const loadMoreBtn = document.querySelector('.work-btn');
loadMoreBtn.addEventListener('click', (event) => {

    const images = loadMoreImages.map((imageDetails) => {
        return workItem(imageDetails);
    });
    const imagesContainer = document.querySelector('.work-img');

    images.forEach((image) => {
        imagesContainer.appendChild(image);
    });

    event.target.remove();

    const activeTab = document.querySelector('.work-navbar-item.active');
    filterTab(activeTab);
});

function workItem(workDetails) {
    const template = `
        <div class="work-img-wrapper">
            <img src="${workDetails.src}" alt="" data-category="${workDetails.category}">
            <div class="img-info">
                <a href="#">
                    <i class="fas fa-link icon-link"></i>
                </a>
                <a href="#">
                    <i class="fas fa-search icon-search"></i>
                </a>
                <div class="info-title">Creative Design</div>
                <div class="info-category">${workDetails.categoryName}</div>
            </div>
        </div>
    `;

    const templateElement = document.createElement('template');
    templateElement.innerHTML = template;

    return document.importNode(templateElement.content, true);
}

//CATEGORY FILTERING

const filteringTabs = document.querySelectorAll('.work-navbar-item');

filteringTabs.forEach(tab => {
    tab.addEventListener('click', onFilteringTabClick);
});

function onFilteringTabClick(event) {

    filteringTabs.forEach(tab => {
        tab.classList.remove('active');
    });

    event.target.classList.add('active');

    filterTab(event.target);
}

function filterTab(tab) {
    const category = tab.dataset.category;

    const images = document.querySelectorAll('.work-img img');

    images.forEach(image => {
        const imgCategory = image.dataset.category;

        if (imgCategory === category || category === 'all') {
            image.classList.remove('work-img-hide');
        } else {
            image.classList.add('work-img-hide');
        }
    });
}



//SECTION FEEDBACK

let galleryThumbs = Array.from(document.querySelectorAll('.gallery-thumb'));

galleryThumbs.forEach(thumb => {
    thumb.addEventListener('click', onGalleryThumbClick);
});

function onGalleryThumbClick(event) {
    selectThumb(event.target);

}

function selectThumb(thumb) {
    const name = thumb.dataset.name;
    const title = thumb.dataset.title;

    const src = thumb.src;

    const iconName = document.querySelector('.icon-name');
    const iconJob = document.querySelector('.icon-job');
    const mainIcon = document.querySelector('.icon');

    iconName.innerText = name;
    iconJob.innerText = title;
    mainIcon.src = src;

}

const galleryArrowRight = document.querySelector('.gallery-arrow-right');

galleryArrowRight.addEventListener('click', () => {
    const mainIcon = document.querySelector('.icon');
    const activeThumb = galleryThumbs.find(item => {
        return item.src === mainIcon.src;
    });

    let activeThumbIndex = galleryThumbs.indexOf(activeThumb);

    const nextThumb = galleryThumbs.length === activeThumbIndex + 1
        ? galleryThumbs[0]
        : galleryThumbs[activeThumbIndex + 1];

    selectThumb(nextThumb);
});

const galleryArrowLeft = document.querySelector('.gallery-arrow-left');

galleryArrowLeft.addEventListener('click', () => {
    const mainIcon = document.querySelector('.icon');
    const activeThumb = galleryThumbs.find(item => {
        return item.src === mainIcon.src;
    });

    let activeThumbIndex = galleryThumbs.indexOf(activeThumb);

    const nextThumb = activeThumbIndex === 0
        ? galleryThumbs[galleryThumbs.length - 1]
        : galleryThumbs[activeThumbIndex - 1];

    selectThumb(nextThumb);
});