function createNewUser() {
    const first = prompt('Enter first name', 'Olya');
    const last = prompt('Enter last name', 'Markysh');
    const birthday = prompt('Enter your birthday date');
    return {
        firstName: first,
        lastName: last,
        birthday: birthday,
        getAge: function() {
            let fullAge = new Date().getFullYear() - new Date(this.birthday).getFullYear();
            return fullAge;
        },
        getLogin: getLogin,
        getPassword: getPassword
    };
    function getLogin() {
        const firstLetter = this.firstName.substr(0,1);
        const loginName = firstLetter + this.lastName;
        return loginName.toLowerCase();
    }
    function getPassword() {
        const firstLetterName = this.firstName.substr(0, 1).toUpperCase();
        const password = firstLetterName + this.lastName + new Date(this.birthday).getFullYear();
        return password;
    }
}
let user = createNewUser();
let userAge = user.getAge();
let userPass = user.getPassword();


console.log(user.getLogin());
console.log(userAge);
console.log(userPass);