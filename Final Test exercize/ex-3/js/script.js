let englishBreakfast = [
    "поджареный бекон", 
    "колбаски", 
    "яичница", 
    "жареные грибы", 
    "жареные помидоры", 
    "фасоль", 
    "хлеб с джемом", 
    "кофе"
];

let dinner = englishBreakfast.slice(4);

englishBreakfast = englishBreakfast.slice(0, 4);

console.log(englishBreakfast, dinner);