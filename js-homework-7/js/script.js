let array = ['one', 'two', 'three', 'four', 'five', 'six', 'seven'];

function newArray(array) {
    let list = document.createElement('ul');

    const arrayListItems = array.map(item => {
        const listItem = document.createElement('li');
        listItem.innerText = item;
        return listItem;
    });

    arrayListItems.forEach(li => {
        list.appendChild(li);
    });
    return list;
}

document.querySelector('script').before(newArray(array));



















