const head = document.head;
const link = document.createElement('link');
link.rel = 'stylesheet';

let theme = localStorage.getItem('themeStyle');

if (theme === 'colorTheme') {
    link.href = 'css/colorTheme.css';
} else {
    link.href = 'css/style.css'
}

head.appendChild(link);

document
    .getElementById('btn')
    .addEventListener('click', onThemeChange);

function onThemeChange() {
    let theme = localStorage.getItem('themeStyle');

    if (theme === 'colorTheme') {
        link.href = 'css/style.css';
        localStorage.setItem('themeStyle', 'default');
    } else {
        link.href = 'css/colorTheme.css';
        localStorage.setItem('themeStyle', 'colorTheme');

    }
}
